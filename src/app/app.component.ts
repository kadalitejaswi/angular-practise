import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'practise-project';
  serverName:string="test server"
  serverCreationStatus= 'No Server is created.';  
  allowNewServer  = false; 
  allowNewServer1 = false ;
  serverCreated = false;
  
  serverID: number = 10;  
  serverStatus: string = 'Offline'; 
  serverStatusChild
  server
  custompipe = "asdxfcgvfdsaSZdxfcgvbh"
  serverDetals($event){
    this.server = $event
  }
  childServer($event){
    this.serverStatusChild = $event;
  }

 
  constructor(){
    this.serverStatus = Math.random() >= 0.5 ? 'Online' : 'Offline';  
    
    setTimeout(()=>{
      this.allowNewServer = true;
      

    },2000)

    
  }
  getServerStatus() {  
    return this.serverStatus;  
    } 
    getColor(){
      this.serverStatus === 'Online' ? 'green' : 'red' ;
    }
 showData(){
  this.allowNewServer = true;
  this.allowNewServer1 =false

  this.serverCreationStatus = "no server created"

 }
 onCreateServer(){
  this.serverCreated = true; 
  this.allowNewServer1 =true
  this.allowNewServer = false;
   this.serverCreationStatus = "server created is :"+this.serverName
 }
 OnUpdateServerName(event: Event) {  
  this.serverName = (<HTMLInputElement>event.target).value;  
}

title1: string ="Top 10 Movies" ;  
    movie: Movie[] =[  
        {title:'Zootopia',
        director:'Rich Moore',
        cast:'Idris Elba',
        releaseDate:'March 4, 2016'},  
        {title:'Batman v Superman: Dawn of Justice',
        director:'Zack Snyder',
        cast:'Ben Affleck ',
        releaseDate:'March 25, 2016'},  
        {title:'Captain America: Civil War'
        ,director:'Anthony Russo',
        cast:'Scarlett Johansson',
        releaseDate:'May 6, 2016'},  
        {title:'X-Men: Apocalypse',director:'Bryan Singer',cast:'Jennifer Lawrence,',releaseDate:'May 27, 2016'},  
    ] 



  } 
    class Movie {  
      title : string;  
      director : string;  
      cast : string;  
      releaseDate : string;  
  } 

