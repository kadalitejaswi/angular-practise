import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { CustomDirectiveDirective } from './directives/custom-directive.directive';
import { CustompipesPipe } from './pipes/custompipes.pipe';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { CustomerDashboardModule } from './customer-dashboard/customer-dashboard.module';
import { ServerModuleModule } from './server-module/server-module.module';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    CustomDirectiveDirective,
    CustompipesPipe,
    ParentComponent,
    ChildComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CustomerDashboardModule,
    ServerModuleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
