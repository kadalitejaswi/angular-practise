import { Directive ,ElementRef ,HostListener,HostBinding, Input,Renderer} from '@angular/core';

@Directive({
  selector: '[appCustomDirective]'
})
export class CustomDirectiveDirective {
@Input() name:string ;
  constructor(private ele:ElementRef , private renderer:Renderer) {  
    ele.nativeElement.style.background = 'red' ;
    ele.nativeElement.style.color = 'white' ;
    ele.nativeElement.style.color = 'white' ;
 }
 @HostBinding('style.border') border: string;
 @HostBinding('style.background') background: string;


 
   @HostListener('mouseover') onMouseOver() {
    this.ChangeBgColor('white');
  this.border = '5px solid green';
  this.background = "yellow"
  

   }
    @HostListener('mouseleave') onMouseLeave() {
      this.ChangeBgColor('black');
      this.border = '5px solid blue';
      this.background = "pink"
  }
  ChangeBgColor(color: string) {
      this.renderer.setElementStyle(this.ele.nativeElement, 'color', color);

  }

//   @HostListener('mouseover') onMouseOver() {
//     this.border = '5px solid green';
// }

   

}
