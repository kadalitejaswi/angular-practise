import { Component, OnInit, Input, Output ,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
@Input() toChild :string;

@Output() messageEvent: EventEmitter <String> = new EventEmitter<String>();
@Output() myDetails: EventEmitter <Object> = new EventEmitter ()
  constructor() { }
  message : string = "hello parent from child";
  wish:string = "hello baby"

  data: object ={
    name: 'tejaswi',
    job:'associate'
  }
  ngOnInit() {
  }


  sendMessage() {
    this.messageEvent.emit(this.message)
  }
  myData() {
    this.myDetails.emit(this.data)
  }
// @Output() nameEvent = new EventEmitter<string>();
// @Output() phoneEvent = new EventEmitter<string>();

// userName: string = '';
// userPhone: string = '';

// onNameChange () {
//   this.nameEvent.emit(this.userName);
// }

// onPhoneChange () {
//   this.phoneEvent.emit(this.userPhone);
// }

}
