import { Component, OnInit, AfterViewInit, ViewChild, Input } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements AfterViewInit {
  @ViewChild(ChildComponent ,{static: false}) child; 
  mydata :object ={name:"dad", age:25}
  newMsg
  newData($event){
    this.newMsg = $event
  }

  myProjectName : string ="parent came to child"
  message:string;
  message2:string;
  myDetails : object ;
  public userName = '';
  public userPhone = '';
  constructor() { }
  ngAfterViewInit(){
    this.message2 = this.child.wish;
  }
  
  projectMethod(){
    console.log("this is parent data");
    
  }

  receiveMessage($event) {
    this.message = $event
  }

  tejaDetails($event){
    this.myDetails = $event ;
  }

  // nameEventHander($event: any) {
  //   this.userName = $event;
  // }

  // phoneEventHander($event: any) {
  //   this.userPhone = $event;
  // }

}
