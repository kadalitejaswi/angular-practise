import { TestBed } from '@angular/core/testing';

import { AppServieService } from './app-servie.service';

describe('AppServieService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppServieService = TestBed.get(AppServieService);
    expect(service).toBeTruthy();
  });
});
