import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-new-comp',
  templateUrl: './new-comp.component.html',
  styleUrls: ['./new-comp.component.css']
})
export class NewCompComponent implements OnInit {
  @Input() newComp:object
  @Output() newmoduleData:EventEmitter< any> = new EventEmitter()
 
  newData:object={company:"virtusa", location :"hyd"}
  constructor() { }

  ngOnInit() {
  }

  data(){
    this.newmoduleData.emit(this.newData)
  }

}
