import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewCompComponent } from './new-comp/new-comp.component';



@NgModule({
  declarations: [NewCompComponent],
  imports: [
    CommonModule
  ],
  exports: [NewCompComponent]
})
export class CustomerDashboardModule { }
