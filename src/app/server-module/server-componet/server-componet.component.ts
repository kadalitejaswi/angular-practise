import { Component, OnInit, Output ,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-server-componet',
  templateUrl: './server-componet.component.html',
  styleUrls: ['./server-componet.component.css']
})
export class ServerComponetComponent implements OnInit {
  @Output() serverDetail : EventEmitter<any> = new EventEmitter()
  @Output() server : EventEmitter<any> = new EventEmitter()

  serverStatus = "running"
  serverName = "VirtusA"

  constructor() { }
  hostData(){
    this.server.emit(this.serverName)
  }
  sendData(){
    this.serverDetail.emit(this.serverStatus)
  }
  ngOnInit() {
  }

}
