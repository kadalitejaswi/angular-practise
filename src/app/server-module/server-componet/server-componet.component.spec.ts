import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerComponetComponent } from './server-componet.component';

describe('ServerComponetComponent', () => {
  let component: ServerComponetComponent;
  let fixture: ComponentFixture<ServerComponetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerComponetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerComponetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
