import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServerComponetComponent } from './server-componet/server-componet.component';



@NgModule({
  declarations: [ServerComponetComponent],
  imports: [
    CommonModule
  ],
  exports:[ServerComponetComponent]

})
export class ServerModuleModule { }
