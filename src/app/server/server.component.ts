import { Component, OnInit } from '@angular/core';
import { AppServieService } from '../services/app-servie.service';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  myTiitle : string = "Practise project"
  todaydate = new Date();  
  serverID: number = 10;  
  serverStatus: string = 'Offline'; 
  jsonval = {name: 'Alex', age: '25', address:{a1: 'Paris', a2: 'France'}} 
  myData: any ;
  // name = "tejaswi"
  // job = "associate"



  
  constructor ( service : AppServieService ) {  
this.myData = service.message
  this.serverStatus = Math.random() > 0.5 ? 'Online' : 'Offline';  
}  



 getServerStatus() {  
  return this.serverStatus ; 
  } 

  getColor() {  
    return this.serverStatus === 'Online' ? 'green' : 'red';  
  }
  ngOnInit() {
  }

}
